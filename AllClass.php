<?php

include 'Student.php';

class AllClass
{
    public function getCountOldStudent($students)
    {
        return count(array_filter($students, function ($student) use ($sex) {
            return ($student->sex == 'male');
            return ($student->age > 19);
        }));
    }

    public function getStudentCountBySex($students, $sex)
    {
        return count(array_filter($students, function ($student) use ($sex) {
            return ($student->sex == $sex);
        }));
    }
    public function getStudentCountByAge($students, $age)
    {
        return count(array_filter($students, function ($student) use ($age) {
            return ($student->age > $age);
        }));
    }
}