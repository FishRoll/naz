<?php
include 'AllClass.php';

$allclass = new AllClass();
$students = array(
    new Student('Sasha', 23, 'female'),
    new Student('Andrew', 18,'male'),
    new Student('Gogi', 20,'female'),
    new Student('Kirya',19,'female'),
    new Student('Jenya',23,'male'),
    new Student('Misha',40,'male'),
);
echo 'К-ть дівчат n-го віку: ', $allclass->getStudentCountBySex($students,'female'), "\n";
echo 'К-ть хлопців за 19 років: ', $allclass->getCountOldStudent($students, 'male'), "\n";
