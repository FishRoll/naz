<?php

include 'C:\Users\Admin\Desktop\naz\AllClass.php';


use  \PHPUnit\Framework\TestCase;

class StudentTest extends TestCase
{
    public function testYearCount()
    {
        $students = array(
            new Student('Sasha', 20,'male'),
            new Student('Andrew', 19, 'male'),
            new Student('Kirya', 24, 'female'),
            new Student('Olya', 35, 'female'),
            new Student('Masha', 21,'female'));
        $age = new AllClass();
        $result = $age->getStudentCountByAge($students, 20);
        $this->assertEquals(20, $result);
    }

    /**
     * @dataProvider countDataProvider
     * @param $actual
     * @param $expected
     */

    public function testCount($students, $expected)
    {
        $result = (new AllClass())->getCountStudent($students);
        $this->assertEquals($expected, $result);
    }

    public function countDataProvider()
    {
        return array(
            array(
                array(
                    new Student('Jane', 20,'female'),
                    new Student ('Tom', 32,'male'),
                    new Student('Tako', 21,'male'),
                ), 20),
        );
    }
}